# Our Group Website

Link: https://misscausal.gitlabpages.inria.fr/misscausal.gitlab.io

The website includes the following sections (and corresponding .Rmd files):
- Homepage (index.Rmd)
- Events: group meetings, reading groups, and other seminars, conferences and summer schools (events.Rmd)
- Members: group members and associated researchers (members.Rmd)
- Research (research.Rmd)
- Software (software.Rmd)
- Publications (publications.Rmd)
- Contact (contact.Rmd)

# How to update the website

The _site.yml file defines the website structure.

1. Open the .Rproj in RStudio
2. Modify the corresponding .Rmd file(s) on your local machine
3. Click `Knit` (or run `rmarkdown::render_site()`) to visualize the changes (first set the working directory to the source file location). If you are satisfied with the result, push the changes to the master repository.
