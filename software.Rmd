```{r echo=F}
title_var <- "Causal Inference and Missing Data Group at Inria"
```    
---
title: `r title_var`
---

# Software {.tabset .tabset-fade .tabset-pills}

## Missing values

Many works of this research group have led to actionable implementations for imputation, estimation, and prediction with incomplete data that are mostly available in R. 

For a comprehensive overview of missing values problems and methods in R and in python we refer to our platform [R-miss-tastic](https://rmisstastic.netlify.com/) (established with [Nathalie Vialaneix](http://www.nathalievialaneix.eu/) and [Nicholas Tierney](https://www.njtierney.com/about/)). 
<br>Related to this platform is the [CRAN Task View on Missing Data](https://cran.r-project.org/web/views/MissingData.html) maintained by members of this group and Nathalie Vialaneix.

**R-packages** developed by former and current group members: 

* [missMDA](https://cran.r-project.org/web/packages/missMDA/index.html): Handling Missing Values with Multivariate Data Analysis (authors: François Husson, Julie Josse)
* [mimi](https://cran.r-project.org/web/packages/mimi/index.html): Main Effects and Interactions in Mixed and Incomplete Data (author: Geneviève Robin)
* [lori](https://cran.r-project.org/web/packages/lori/index.html): Imputation of High-Dimensional Count Data using Side Information (author: Geneviève Robin)
* [misaem](https://cran.r-project.org/web/packages/misaem/index.html): Linear Regression and Logistic Regression with Missing Covariates (author: Wei Jiang)

**Other implementations** developed inside our group:

* [PPCA_MNAR](https://github.com/AudeSportisse/PPCA_MNAR) (R code): Estimation and imputation in Probabilistic Principal Component Analysis with Missing Not At Random data (author: Aude Sportisse)
* [SGD-NA](https://github.com/AudeSportisse/SGD-NA) (Python code): Debiasing Stochastic Gradient Descent with Missing Completely At Random data (author: Aude Sportisse)


## Causal inference


Several implementations have been proposed by members of this group to tackle treatment effect estimation with incomplete attributes and on combined experimental and observational data:

* [causal-inference-missing](https://github.com/imkemayer/causal-inference-missing) (R code):  Doubly robust treatment effect estimation with missing attributes (author: Imke Mayer)
* [combine-rct-rwd](https://gitlab.inria.fr/misscausal/combine-rct-rwd-review) (R code):  Causal inference methods for combining randomized controlled trials and observational studies (authors: Bénédicte Colnet, Imke Mayer)

Additionally, we have created the [CRAN Task View on Causal Inference](https://cran.r-project.org/web/views/CausalInference.html) (maintainers: Pan Zhao, Imke Mayer, et al.). It provides an overview of implementations of causal inference and causal discovery methods currently available on [CRAN (The Comprehensive R Archive Network)](https://cran.r-project.org/). If you are intersted in contributing or have feedback on this task view, please reach out to the task view maintainers.


## Uncertainty quantification

Numerous endeavors from this research group have resulted in practical applications for uncertainty quantification techniques, including the development of methodologies in conformal prediction.

* R package [AdaptiveConformal](https://github.com/herbps10/AdaptiveConformal) implements several Adaptive Conformal Inference (ACI) algorithms.
* Other implementations are available at [Margaux's GitHub](https://github.com/mzaffran).

## Health applications

Application for bed allocation monitoring: [ICUBAM](https://icubam.github.io/)
<br>
<p class="tab"> *ICUBAM provides real-time monitoring of intensive care unit (ICU) bed availability in French hospitals. Data is directly obtained from doctors working inside ICU by sending them SMS with a HTTP link to a form that they can fill in 15 seconds.*
<br>
*The project was co-built by ICU Doctors from CHRU Nancy/Université de Loraine and engineers from INRIA & Polytechnique. It was fleshed out live during the Covid crisis in Eastern France to answer an urgent need for finding available ICU beds in a saturated and deteriorating situation. At the time of writing, 5 engineers are working full-time, 7 days a week, on the project, in direct contact with the team of ICU doctors on the ground.*</p>

<style type="text/css">
.tab { margin-left: 20px; }
</style>
  
<style>
  .title{
    display: none;
  }
</style>
